import email
import imaplib
import sqlite3
from email.header import decode_header

from bs4 import BeautifulSoup

LOGIN = 'temirbayev02@mail.ru'
PASSWORD = 'uK2i2igsUDyqcH8jxPLj'
PORT = 993
SERVER = 'imap.mail.ru'
DB = 'qwe.db'


class MailAnalyzer:
    def __init__(self, username, password, server, port, db_file):
        """
        :param username:
        :param password:
        :param server:
        :param port:
        :param db_file:
        """
        self.username = username
        self.password = password
        self.server = server
        self.port = port
        self.db_file = db_file
        self.connection = None
        self.mail = None

    def connect(self):
        try:
            self.mail = imaplib.IMAP4_SSL(self.server, self.port)
            self.mail.login(self.username, self.password)
            print("Connected to Mail.ru server.")
        except Exception as e:
            print(f"Error connecting to Mail.ru server: {e}")

    def connect_to_database(self):
        try:
            self.connection = sqlite3.connect(self.db_file)
            print("Connected to the database.")

        except Exception as e:
            print(f"Error connecting to the database: {e}")

    def latest_message(self):
        """
        :return: latest_message - показывает последнее сообщение со всеми аттрибутами
        """
        if not self.mail:
            print('Not connected to the server def_recipient')
            return

        self.mail.select('inbox')
        status, messages = self.mail.search(None, 'ALL')

        if status == 'OK':
            # Проверка, есть ли хотя бы одно сообщение
            if messages[0]:
                # Получение последнего сообщения
                latest_msg_id = messages[0].split()[-1]
                status, msg_data = self.mail.fetch(latest_msg_id, '(RFC822)')
                latest_msg = email.message_from_bytes(msg_data[0][1])

                # Теперь latest_msg содержит последнее сообщение
                # Вывод информации о последнем сообщении

                print("Subject:", decode_header(latest_msg['Subject'])[0][0].decode())
                print("From:", latest_msg['From'])
                print("Date:", latest_msg['Date'])
                print("Email:", latest_msg['Return-path'])
                print("ID-mail:", latest_msg['Message-ID'])
                # Получение текста сообщения
                # -----------------------------------------------------------------------------------------
                for part in latest_msg.walk():
                    if part.get_content_maintype() == 'text' and part.get_content_subtype() == 'html':
                        # Тут получаем текст вместе с html
                        text = part.get_payload()
                        # Снизу убрали весь html код
                        soup = BeautifulSoup(text, 'html.parser')
                        text_content = soup.get_text()
                        print(text_content)
                # -----------------------------------------------------------------------------------------

    def all_message(self):
        """
        :return: all_message - для того чтобы вывести все сообщения на почте как:
                Subject: ВАЖНО                              - тема сообщения
                From: Temirlan <temirbayev002@gmail.com>    - и почта и имя отправителя
        """
        if not self.mail:
            print("Not connected to the server.")
            return

        # Выбор папки в почтовом ящике (например, 'INBOX')
        self.mail.select('inbox')

        # Поиск всех сообщений в папке
        status, messages = self.mail.search(None, 'ALL')

        if status == 'OK':
            for msg_id in messages[0].split():
                status, msg_data = self.mail.fetch(msg_id, '(RFC822)')
                msg = email.message_from_bytes(msg_data[0][1])

                subject = decode_header(msg['Subject'])[0][0]
                if isinstance(subject, bytes):
                    subject = subject.decode('utf-8')
                print(f"Subject: {subject}")

                sender = msg.get('From')
                print(f"From: {sender}")

    def check_user(self, user: str):
        """
        :param user: @mail
        :return: Check_user для ввода в БД нового пользователя по которому будет поиск
        если не вводить пользователя то будет поиск по данным которые есть в БД
        выводит
        From: Temirlan <temirbayev002@gmail.com>                       - выводит от кого пришло сообщение с именем
        Date: Tue, 5 Dec 2023 01:43:59 +0600                           - дату получения
        Email: <temirbayev002@gmail.com>                               - выводит email
        ID-mail: <481C3FD7-F3BF-4D93-B96F-B88FD88FC3CF@hxcore.ol>      - id сообщения
        """
        if not self.mail:
            print('Not connected to the server check_user')
            return

        cur = self.connection.cursor()

        if user:
            cur.execute("INSERT INTO User (login) VALUES (?)", (user,))
            self.connection.commit()
            print(f'{user}, сохранили в базе данных')
        else:
            self.mail.select('inbox')
            status, messages = self.mail.search(None, 'ALL')

            cur.execute('select login from user')
            result = cur.fetchall()
            for raw in result:
                print(f'будем искать их: {raw[0]}')
                user = '<' + raw[0] + '>'
                if status == 'OK':
                    for msg_id in messages[0].split():
                        status, msg_data = self.mail.fetch(msg_id, '(RFC822)')
                        msg = email.message_from_bytes(msg_data[0][1])
                        if msg['Return-path'] == user:
                            print("From:", msg['From'])
                            print("Date:", msg['Date'])
                            print("Email:", msg['Return-path'])
                            print("ID-mail:", msg['Message-ID'])
                            print(' ')
                        else:
                            print('Dont find this user')

    def check_message(self):
        """
        :return: check_message - для того чтобы выявить по теме 'ВАЖНО'
        """
        if not self.mail:
            print('Not connected to the server check_message')
            return

        self.mail.select('inbox')
        status, message = self.mail.search(None, 'ALL')

        if status == 'OK':
            for msg_id in message[0].split():
                status, msg_data = self.mail.fetch(msg_id, '(RFC822)')
                msg = email.message_from_bytes(msg_data[0][1])
                try:
                    subject = decode_header(msg['Subject'])[0][0].decode()
                    if subject == 'ВАЖНО':
                        print("From:", msg['From'])
                        print("Date:", msg['Date'])
                        print("Email:", msg['Return-path'])
                        print("ID-mail:", msg['Message-ID'])
                        print(' ')
                    else:
                        pass
                except Exception as e:
                    print(f'Problem: {e}')
                    print(' ')

    def find_with_id(self, id):
        """
        :param id: id - сообщения пример - ID-mail: <38CBF013-F504-421C-A531-93CD02C01961@hxcore.ol>
        :return: сообщение которое хотели получить по id
        """
        if not self.mail:
            print('Not connected to the server check_message')
            return

        self.mail.select('inbox')
        status, message = self.mail.search(None, 'ALL')

        if status == 'OK':
            for msg_id in message[0].split():
                status, msg_data = self.mail.fetch(msg_id, '(RFC822)')
                msg = email.message_from_bytes(msg_data[0][1])
                if msg['Message-ID'] == id:
                    print("Subject:", decode_header(msg['Subject'])[0][0].decode())
                    print("From:", msg['From'])
                    print("Date:", msg['Date'])
                    print("Email:", msg['Return-path'])
                    print("ID-mail:", msg['Message-ID'])
                    # Получение текста сообщения
                    # -----------------------------------------------------------------------------------------
                    for part in msg.walk():
                        if part.get_content_maintype() == 'text' and part.get_content_subtype() == 'html':
                            # Тут получаем текст вместе с html
                            text = part.get_payload()
                            # Снизу убрали весь html код
                            soup = BeautifulSoup(text, 'html.parser')
                            text_content = soup.get_text()
                            print(text_content)
                    # -----------------------------------------------------------------------------------------

    def close_connection(self):
        if self.mail:
            self.mail.logout()
            print("Connection closed.")
        else:
            print("Not connected to the server.")

        if self.connection:
            self.connection.close()
            print('DB closed')
        else:
            print('Not connected DB')

    def example(self):
        # self.mail.select('inbox')
        #
        # # Поиск всех сообщений в папке
        # status, messages = self.mail.search(None, 'ALL')
        # for msg_id in messages[0].split():
        #     status, msg_data = self.mail.fetch(msg_id, '(RFC822)')
        #     msg = email.message_from_bytes(msg_data[0][1])
        #     print(msg['Return-path'])
        cur = self.connection.cursor()
        cur.execute('select login from user')
        result = cur.fetchall()
        for raw in result:
            print('<' + raw[0] + '>')
            print()


# Использование класса
if __name__ == '__main__':
    mail_analyzer = MailAnalyzer(LOGIN, PASSWORD, SERVER, PORT, DB)

    mail_analyzer.connect()
    mail_analyzer.connect_to_database()

    # mail_analyzer.find_with_id('<38CBF013-F504-421C-A531-93CD02C01961@hxcore.ol>')
    # mail_analyzer.example()
    # mail_analyzer.check_user(input('input your email: '))
    # mail_analyzer.connect_to_database()
    # mail_analyzer.all_message()
    # mail_analyzer.latest_message()
    # mail_analyzer.check_message()

    mail_analyzer.close_connection()
