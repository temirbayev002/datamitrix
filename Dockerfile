# Используем официальный образ Python в качестве базового
FROM python:3.11.0

# Устанавливаем рабочую директорию /app
WORKDIR /app

# Копируем содержимое текущей директории в контейнер в /app
COPY . /app

# Устанавливаем необходимые пакеты из requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Запускаем mail_analyzer.py при запуске контейнера
CMD ["python", "main.py"]
